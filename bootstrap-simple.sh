#!/bin/bash

pkg_mgr=
installed=

if command -v apt 2>/dev/null; then
    pkg_mgr="apt"
    installed="--installed"
elif command -v dnf 2>/dev/null; then
    pkg_mgr="dnf"
    installed="installed"
elif command -v yum 2>/dev/null; then
    pkg_mgr="yum"
    installed="installed"
else
    echo "OS not supported"
    exit 1
fi

if ! sudo ${pkg_mgr} list ${installed} ansible >/dev/null 2>&1; then
    sudo ${pkg_mgr} install ansible -y
fi
